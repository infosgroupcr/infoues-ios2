//
//  ContactBottomButtonsTableViewCell.swift
//  infoUes
//
//  Created by aivatra on 8/20/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import UIKit

class ContactBottomButtonsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var buttonTop: GenericButton!
    @IBOutlet weak var buttonMiddle: GenericButton!
    @IBOutlet weak var buttonBottom: GenericButton!

    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let stringManager = StringInjectorHelper()
        
        self.buttonTop.flatButton(titleText: stringManager.getStringFromPList(sectionName: "viewContact", stringName: "buttonLabelGeneralInfo"), titleTextColor: "buttonTextLabelWhite", buttonColor: "buttonBackgroundColorLightBlue")
        self.buttonMiddle.flatButton(titleText: stringManager.getStringFromPList(sectionName: "viewContact", stringName: "buttonCareers"), titleTextColor: "buttonTextLabelWhite", buttonColor: "buttonBackgroundColorPurple")
        self.buttonBottom.flatButton(titleText: stringManager.getStringFromPList(sectionName: "viewContact", stringName: "buttonStudentServices"), titleTextColor: "buttonTextLabelWhite", buttonColor: "buttonBackgroundColorPurple")

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
