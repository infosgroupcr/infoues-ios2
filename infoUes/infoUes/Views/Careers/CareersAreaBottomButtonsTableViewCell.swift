//
//  CareersAreaBottomButtonsTableViewCell.swift
//  infoUes
//
//  Created by aivatra on 8/10/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import UIKit

class CareersAreaBottomButtonsTableViewCell: UITableViewCell {
    @IBOutlet weak var buttonTop: GenericButton!

    @IBOutlet weak var buttonBottom: GenericButton!
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let stringManager = StringInjectorHelper()
        
        self.buttonTop.flatButton(titleText: stringManager.getStringFromPList(sectionName: "viewCareers", stringName: "buttonLabelContact"), titleTextColor: "buttonTextLabelWhite", buttonColor: "buttonBackgroundColorLightBlue")
        self.buttonBottom.flatButton(titleText: stringManager.getStringFromPList(sectionName: "viewCareers", stringName: "buttonStudentServices"), titleTextColor: "buttonTextLabelWhite", buttonColor: "buttonBackgroundColorPurple")

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
