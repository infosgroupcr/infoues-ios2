//
//  CareersAliasDetailBottomButtonsTableViewCell.swift
//  infoUes
//
//  Created by aivatra on 8/10/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import UIKit

class CareersAliasDetailBottomButtonsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var buttonBack: GenericButton!
    @IBOutlet weak var buttonGeneralInfo: GenericButton!
    @IBOutlet weak var buttonCareers: GenericButton!
    @IBOutlet weak var buttonStudentServices: GenericButton!
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let stringManager = StringInjectorHelper()
        
        self.buttonBack.flatButton(titleText: stringManager.getStringFromPList(sectionName: "viewCareers", stringName: "buttonLabelBack"), titleTextColor: "buttonTextLabelPurple", buttonColor: "buttonBackgroundColorWhite")
        self.buttonGeneralInfo.flatButton(titleText: stringManager.getStringFromPList(sectionName: "viewCareers", stringName: "buttonLabelGeneralInfo"), titleTextColor: "buttonTextLabelWhite", buttonColor: "buttonBackgroundColorLightBlue")
        self.buttonCareers.flatButton(titleText: stringManager.getStringFromPList(sectionName: "viewCareers", stringName: "buttonLabelCareers"), titleTextColor: "buttonTextLabelWhite", buttonColor: "buttonBackgroundColorLightBlue")
        self.buttonStudentServices.flatButton(titleText: stringManager.getStringFromPList(sectionName: "viewCareers", stringName: "buttonStudentServices"), titleTextColor: "buttonTextLabelWhite", buttonColor: "buttonBackgroundColorPurple")

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
