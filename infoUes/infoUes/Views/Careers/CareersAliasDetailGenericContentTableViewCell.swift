//
//  CareerAliasDetailGenericContentTableViewCell.swift
//  infoUes
//
//  Created by aivatra on 8/11/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import UIKit

class CareersAliasDetailGenericContentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var imageChevron: UIImageView!
    @IBOutlet weak var labelSubtitle: UILabel!
    @IBOutlet weak var imagePhone: UIImageView!
    @IBOutlet weak var imageEmail: UIImageView!
    @IBOutlet weak var imageWeb: UIImageView!
    @IBOutlet weak var hiddenViewConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var imageAcred1: UIImageView!
    @IBOutlet weak var imageAcred2: UIImageView!
    @IBOutlet weak var imageAcred3: UIImageView!
    
    var isObserving = false;
    
    class var expandedHeight: CGFloat { get { return 275 } }
    class var defaultHeight: CGFloat  { get { return 80  } }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.imagePhone.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.goToContactDetail)))
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func checkHeight() {
        if frame.size.height < CareersAliasDetailGenericContentTableViewCell.expandedHeight {
            self.imageChevron.image = #imageLiteral(resourceName: "ChevronCollapsed")
        }else {
            self.imageChevron.image = #imageLiteral(resourceName: "ChevronExpanded")
        }
        self.imagePhone.isHidden = (frame.size.height < CareersAliasDetailGenericContentTableViewCell.expandedHeight)
        self.imageEmail.isHidden = (frame.size.height < CareersAliasDetailGenericContentTableViewCell.expandedHeight)
        self.imageWeb.isHidden = (frame.size.height < CareersAliasDetailGenericContentTableViewCell.expandedHeight)
        self.labelSubtitle.isHidden = (frame.size.height < CareersAliasDetailGenericContentTableViewCell.expandedHeight)
        self.imageAcred1.isHidden = (frame.size.height < CareersAliasDetailGenericContentTableViewCell.expandedHeight)
        self.imageAcred2.isHidden = (frame.size.height < CareersAliasDetailGenericContentTableViewCell.expandedHeight)
        self.imageAcred3.isHidden = (frame.size.height < CareersAliasDetailGenericContentTableViewCell.expandedHeight)
    }
    
    func watchFrameChanges() {
        if !isObserving {
            addObserver(self, forKeyPath: "frame", options: [NSKeyValueObservingOptions.new, NSKeyValueObservingOptions.initial], context: nil)
            isObserving = true;
        }
    }
    
    func ignoreFrameChanges() {
        if isObserving {
            removeObserver(self, forKeyPath: "frame")
            isObserving = false;
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "frame" {
            checkHeight()
        }
    }
    
    func goToContactDetail(sender:UITapGestureRecognizer) {
        print("inside gesture recognizer!")
    }
    
    deinit {
        ignoreFrameChanges()
    }
    
}
