//
//  CareersHeaderTableViewCell.swift
//  infoUes
//
//  Created by aivatra on 8/8/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import UIKit

class CareersHeaderTableViewCell: UITableViewCell {
    
   
    @IBOutlet weak var viewButton1: UIView!
    @IBOutlet weak var viewButtonImage1: UIImageView!
    @IBOutlet weak var viewButton2: UIView!
    @IBOutlet weak var viewButtonImage2: UIImageView!
    @IBOutlet weak var viewButton3: UIView!
    @IBOutlet weak var viewButtonImage3: UIImageView!
    @IBOutlet weak var viewButton4: UIView!
    @IBOutlet weak var viewButtonImage4: UIImageView!
    @IBOutlet weak var viewButton5: UIView!
    @IBOutlet weak var viewButtonImage5: UIImageView!
    @IBOutlet weak var viewButton6: UIView!
    @IBOutlet weak var viewButtonImage6: UIImageView!
    @IBOutlet weak var viewButton7: UIView!
    @IBOutlet weak var viewButtonImage7: UIImageView!
    @IBOutlet weak var viewButton8: UIView!
    @IBOutlet weak var viewButtonImage8: UIImageView!
    @IBOutlet weak var viewButton9: UIView!
    @IBOutlet weak var viewButtonImage9: UIImageView!
    
    @IBOutlet weak var viewButtonsContainer: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageHand: UIImageView!
    @IBOutlet weak var imageHeader: UIImageView!
    @IBOutlet weak var viewContent: UIView!
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.viewButtonsContainer.layer.cornerRadius = 15
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
