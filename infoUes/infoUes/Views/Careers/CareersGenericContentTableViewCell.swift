//
//  CareersGenericContentTableViewCell.swift
//  infoUes
//
//  Created by aivatra on 8/8/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import UIKit

class CareersGenericContentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelSkilsTitle: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var imageHeader: UIImageView!
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var textViewSkills: UITextView!
    @IBOutlet weak var buttonGoToCareerDetail: GenericButton!
    @IBOutlet weak var buttonUp: GenericButton!
    
    var rowType: Double!
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let stringManager = StringInjectorHelper()
        
        self.buttonUp.goToTopButton(titleText: stringManager.getStringFromPList(sectionName: "viewCareers", stringName: "buttonLabelGoUp"), titleTextColor: "buttonTextLabelPurple", buttonColor: "buttonBackgroundColorWhite", image: #imageLiteral(resourceName: "careersChevron2"))
        
        self.buttonGoToCareerDetail.attributedButton(titleText: stringManager.getStringFromPList(sectionName: "viewCareers", stringName: "textSubtitleAliases"), titleTextColor: "buttonTextLabelPurple", buttonColor: "buttonBackgroundColorWhite", borderColor: "buttonBackgroundColorPurple", image: #imageLiteral(resourceName: "careerChevronRight3"))


    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
