//
//  GeneralInfoBottomButtonsTableViewCell.swift
//  infoUes
//
//  Created by aivatra on 8/7/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import UIKit

class GeneralInfoBottomButtonsTableViewCell: UITableViewCell {

    @IBOutlet weak var buttonBottom: GenericButton!
    @IBOutlet weak var buttonTop: GenericButton!
    @IBOutlet weak var viewContent: UIView!
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let stringManager = StringInjectorHelper()
        
        self.buttonTop.flatButton(titleText: stringManager.getStringFromPList(sectionName: "viewGeneralInfo", stringName: "buttonCareers"), titleTextColor: "buttonTextLabelWhite", buttonColor: "buttonBackgroundColorLightBlue")
        // TODO: Fix buttonBottom
        self.buttonBottom.flatButton(titleText: stringManager.getStringFromPList(sectionName: "viewGeneralInfo", stringName: "buttonStudentServices"), titleTextColor: "buttonTextLabelPurple", buttonColor: "buttonBackgroundColorWhite")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
