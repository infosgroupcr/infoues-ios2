//
//  GeneralInfoGenericContentTableViewCell.swift
//  infoUes
//
//  Created by aivatra on 8/7/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import UIKit

class GeneralInfoGenericContentTableViewCell: UITableViewCell {

    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var imageChevron: UIImageView!
    @IBOutlet weak var labelMore: UILabel!
    
    var isObserving = false;
    
    class var expandedHeight: CGFloat { get { return 600 } }
    class var defaultHeight: CGFloat  { get { return 170  } }

    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func checkHeight() {
        if frame.size.height < GeneralInfoGenericContentTableViewCell.expandedHeight {
            self.imageChevron.image = #imageLiteral(resourceName: "ChevronCollapsed")
        }else {
            self.imageChevron.image = #imageLiteral(resourceName: "ChevronExpanded")
        }
//        self.imagePhone.isHidden = (frame.size.height < CareersAliasDetailGenericContentTableViewCell.expandedHeight)
//        self.imageEmail.isHidden = (frame.size.height < CareersAliasDetailGenericContentTableViewCell.expandedHeight)
//        self.imageWeb.isHidden = (frame.size.height < CareersAliasDetailGenericContentTableViewCell.expandedHeight)
//        self.labelSubtitle.isHidden = (frame.size.height < CareersAliasDetailGenericContentTableViewCell.expandedHeight)
//        self.imageAcred1.isHidden = (frame.size.height < CareersAliasDetailGenericContentTableViewCell.expandedHeight)
//        self.imageAcred2.isHidden = (frame.size.height < CareersAliasDetailGenericContentTableViewCell.expandedHeight)
//        self.imageAcred3.isHidden = (frame.size.height < CareersAliasDetailGenericContentTableViewCell.expandedHeight)
    }
    
    func watchFrameChanges() {
        if !isObserving {
            addObserver(self, forKeyPath: "frame", options: [NSKeyValueObservingOptions.new, NSKeyValueObservingOptions.initial], context: nil)
            isObserving = true;
        }
    }
    
    func ignoreFrameChanges() {
        if isObserving {
            removeObserver(self, forKeyPath: "frame")
            isObserving = false;
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "frame" {
            checkHeight()
        }
    }
    
    deinit {
        ignoreFrameChanges()
    }
    
}
