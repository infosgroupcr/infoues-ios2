//
//  AdmissionProcessBottomButtonsTableViewCell.swift
//  infoUes
//
//  Created by aivatra on 8/7/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import UIKit

class AdmissionProcessBottomButtonsTableViewCell: UITableViewCell {

    @IBOutlet weak var buttonBottom: GenericButton!
    @IBOutlet weak var buttonTop: GenericButton!
    @IBOutlet weak var viewContent: UIView!
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let stringManager = StringInjectorHelper()
        
        self.buttonTop.flatButton(titleText: stringManager.getStringFromPList(sectionName: "viewAdmissionProcess", stringName: "buttonLabelDetails"), titleTextColor: "buttonTextLabelWhite", buttonColor: "buttonBackgroundColorLightBlue")
        self.buttonBottom.flatButton(titleText: stringManager.getStringFromPList(sectionName: "viewAdmissionProcess", stringName: "buttonLabelContact"), titleTextColor: "buttonTextLabelWhite", buttonColor: "buttonBackgroundColorPurple")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
