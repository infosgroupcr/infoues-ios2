//
//  StudentServicesGenericContentTableViewCell.swift
//  infoUes
//
//  Created by aivatra on 8/7/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import UIKit

class StudentServicesGenericContentTableViewCell: UITableViewCell {

    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
