//
//  FakeSplashViewController.swift
//  infoUes
//
//  Created by aivatra on 8/21/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import UIKit

class FakeSplashViewController: UIViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // MARK: Test APIService
        let service = APIService()
        let coreDataHelper = CoreDataHelper()
        self.activityIndicator.startAnimating()
        
        if ((UserDefaults.standard.value(forKey: "lastTimeDataSaved") as? Date) != nil) {
            self.getDataFromAPI()
        }else {
            
            service.getData { (result) in
                switch result {
                case .Success(let data):
                    coreDataHelper.saveInCoreDataWith(array: data)
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.isHidden = true
                    self.goToMain()
                case .Error(let message):
                    DispatchQueue.main.async {
                        print("Error: \(message)")
                    }
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getDataFromAPI(){
        
        let service = APIService()
        let coreDataHelper = CoreDataHelper()
        service.getChecksum { (result) in
            
            switch result {
            case .Success(let data):
                if let dateReturned = data["fecha"] as? String {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss"
                    let checksumDateFromJSON = dateFormatter.date(from: dateReturned)!
                    
                    if let checksumDateFromDefaults = UserDefaults.standard.value(forKey: "lastTimeDataSaved") as? Date {
                        if checksumDateFromJSON > checksumDateFromDefaults {
                            service.getData { (result) in
                                switch result {
                                case .Success(let data):
                                    coreDataHelper.saveInCoreDataWith(array: data)
//                                    self.activityIndicator.stopAnimating()
//                                    self.activityIndicator.isHidden = true
//                                    self.goToMain()
                                case .Error(let message):
                                    DispatchQueue.main.async {
                                        print("Error: \(message)")
                                    }
                                }
                            }
                        }
                    }
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.isHidden = true
                    self.goToMain()
                }
            case .Error(let message):
                DispatchQueue.main.async {
                    print("Error: \(message)")
                }
            }
        }
    }
    
    func goToMain(){
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc : MainViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        
        let navigationController = UINavigationController(rootViewController: vc)
        self.present(navigationController, animated: false, completion: nil)
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
