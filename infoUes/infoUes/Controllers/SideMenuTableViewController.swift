import Foundation
import SideMenu

class SideMenuTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    // MARK: Outlets
    
    @IBOutlet weak var closeButton: UIBarButtonItem!
    @IBOutlet weak var viewTitleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var searchButton: GenericButton!
    // MARK: Attributes
    private var myArray = [String]()
    
    // MARK: View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.alwaysBounceVertical = false
        self.tableView.isHidden = false
        self.searchBar.delegate = self
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SideMenuTableViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let colorHelper = ColorInjectorHelper()
        let stringManager = StringInjectorHelper()
        
        self.myArray = stringManager.getArrayFromPList(sectionName: "viewMenu", stringName: "menuOptions")
        
        let backgroundColor = colorHelper.getColorFromPList(colorName: "viewBackgroundColorPurple")
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController!.view.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        self.backgroundView.backgroundColor = backgroundColor
        
        self.searchBar.placeholder = stringManager.getStringFromPList(sectionName: "viewMenu", stringName: "labelSearchPlaceholder")
        self.searchBar.backgroundColor = UIColor.white
        self.searchBar.layer.cornerRadius = 23
        self.searchButton.isHidden = true
        
        self.searchButton.flatButton(titleText: stringManager.getStringFromPList(sectionName: "viewMenu", stringName: "buttonLabelSearch"), titleTextColor: "buttonTextLabelWhite", buttonColor: "buttonBackgroundColorLightBlue")

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("SideMenu Appeared!")
        tableView.frame = CGRect(x: tableView.frame.origin.x, y: tableView.frame.origin.y, width: tableView.frame.size.width, height: tableView.contentSize.height)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        print("SideMenu Disappearing!")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        print("SideMenu Disappeared!")
    }
    
    // MARK: Actions

    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        print("Num: \(indexPath.row)")
//        print("Value: \(myArray[indexPath.row])")
        
        switch indexPath.row {
        case 0:
//            let cr = storyboard.instantiateViewController(withIdentifier: "GeneralInfoViewController")
//            self.navigationController?.pushViewController(cr, animated: true)
            let storyboard = UIStoryboard(name: "GeneralInfo", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "GeneralInfoViewController")
            self.navigationController?.pushViewController(controller, animated: true)
            break
        case 1:
            let storyboard = UIStoryboard(name: "Careers", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "CareersViewController")
            self.navigationController?.pushViewController(controller, animated: true)
            break
        case 2:
            let storyboard = UIStoryboard(name: "StudentServices", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "StudentServicesViewController")
            self.navigationController?.pushViewController(controller, animated: true)
            break
        case 3:
            let storyboard = UIStoryboard(name: "AdmissionProcess", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "AdmissionProcessViewController")
            self.navigationController?.pushViewController(controller, animated: true)
            break
        case 4:
//            let storyboard = UIStoryboard(name: "Contact", bundle: nil)
//            let controller = storyboard.instantiateViewController(withIdentifier: "ContactViewController")
//            self.navigationController?.pushViewController(controller, animated: true)
            break
        default:
            break
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)
        cell.textLabel!.text = "\(myArray[indexPath.row])"
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.textColor = UIColor.white
        cell.contentView.backgroundColor = UIColor.clear
        cell.backgroundColor = UIColor.clear
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        cell.selectedBackgroundView = backgroundView
//        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell
    }
    
    override func viewDidLayoutSubviews(){
        tableView.frame = CGRect(x: tableView.frame.origin.x, y: tableView.frame.origin.y, width: tableView.frame.size.width, height: tableView.contentSize.height)
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        // TODO: Search functionality goes here
        self.tableView.isHidden = true
        self.searchButton.isHidden = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.tableView.isHidden = false
        self.searchButton.isHidden = true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchBar.isHidden = false
    }
    

    func dismissKeyboard() {
        view.endEditing(true)
        if self.searchBar.text == "" {
            self.tableView.isHidden = false
            self.searchButton.isHidden = true
        }

        self.tableView.isUserInteractionEnabled = true
    }
    
    @IBAction func searchCareers(_ sender: Any) {
        print("searchButton pressed!")
    }
    
}
