//
//  CareerAliasViewController.swift
//  infoUes
//
//  Created by aivatra on 8/7/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import UIKit
import CoreData

struct CareerAliasRow {
    static let headerRow = CareersAliasHeaderTableViewCell()
    static let contentRow = CareersAliasGenericContentTableViewCell()
    static let bottomButtonsRow = CareersAliasBottomButtonsTableViewCell()
}

class CareerAliasViewController: UITableViewController {

    var universitiesByCareer = NSArray()
    var careeAliasId: Double!
    var careerAliasName: String!
    var rowsData = [UITableViewCell]()
    var universityData: Universidades!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        print(universitiesByCareer)
        super.viewDidLoad()
        self.tableView.estimatedRowHeight = 80
        self.tableView.rowHeight = UITableViewAutomaticDimension
        tableView?.register(CareersAliasHeaderTableViewCell.nib, forCellReuseIdentifier: CareersAliasHeaderTableViewCell.identifier)
        tableView?.register(CareersAliasGenericContentTableViewCell.nib, forCellReuseIdentifier: CareersAliasGenericContentTableViewCell.identifier)
        tableView?.register(CareersAliasBottomButtonsTableViewCell.nib, forCellReuseIdentifier: CareersAliasBottomButtonsTableViewCell.identifier)
        self.getData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getData() {
        
        rowsData = [CareerAliasRow.headerRow]
        for _ in universitiesByCareer {
            rowsData += [CareerAliasRow.contentRow]
        }
        rowsData += [CareerAliasRow.bottomButtonsRow]
        
    }
    
    func getUniversidadData(index: Int) -> Universidades{
        if let jsonDataArray = (universitiesByCareer[index] as! NSDictionary) as? [String: Any]{
            
            let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Universidades")
            let predicate = NSPredicate(format: "id == %@", String((jsonDataArray["idUniversidad"] as? Int)!))
            fetchRequest.predicate = predicate
            
            do {
                let results = try context.fetch(fetchRequest) as! [Universidades]
                if let data = results.first {
                    universityData = data
                }
            }catch let err as NSError {
                print(err.debugDescription)
            }
            
        }
        return universityData
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowsData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        let stringManager = StringInjectorHelper()
        let colorManager = ColorInjectorHelper()
//        var imageHeader = UIImage()

        if rowsData[indexPath.row] == CareerAliasRow.headerRow {
            
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CareersAliasHeaderTableViewCell", for: indexPath) as! CareersAliasHeaderTableViewCell
            if let careerAlias: String? = careerAliasName {
                cell.labelTitle.text = careerAlias!
                cell.labelTitle.textColor = colorManager.getColorFromPList(colorName: "textLabelLightBlue")
            }
            cell.layer.backgroundColor = UIColor.clear.cgColor
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            return cell
            
        }else if rowsData[indexPath.row] == CareerAliasRow.contentRow{
            let imageHelper = ImageConverterHelper()
            // TODO: mostrar la imagen de la universidad
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CareersAliasGenericContentTableViewCell", for: indexPath) as! CareersAliasGenericContentTableViewCell
            if let jsonDataArray = (universitiesByCareer[indexPath.row - 1] as! NSDictionary) as? [String: Any]{
                cell.labelTitle.text = jsonDataArray["nombre"] as? String
                cell.idUniversity = jsonDataArray["idUniversidad"] as? Int
                cell.imageUniversityIcon.image = imageHelper.convertImage(imageString: self.getUniversidadData(index: indexPath.row - 1).foto!)
            }
           
            cell.layer.backgroundColor = UIColor.clear.cgColor
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
            
        }else if rowsData[indexPath.row] == CareerAliasRow.bottomButtonsRow{
            if let cell = self.tableView.dequeueReusableCell(withIdentifier: "CareersAliasBottomButtonsTableViewCell", for: indexPath) as? CareersAliasBottomButtonsTableViewCell{
                cell.layer.backgroundColor = UIColor.clear.cgColor
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            }
        }
        
        return UITableViewCell()
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if rowsData[indexPath.row] == CareerAliasRow.contentRow{
            let careerAliasDetailViewController = storyboard?.instantiateViewController(withIdentifier: "CareerAliasDetailViewController") as! CareerAliasDetailViewController
            if let jsonDataArray = (universitiesByCareer[indexPath.row - 1] as! NSDictionary) as? [String: Any]{
                careerAliasDetailViewController.aliasDetailName = jsonDataArray["nombre"] as? String
                careerAliasDetailViewController.aliasDetailDescription = jsonDataArray["descripcion"] as? String
                careerAliasDetailViewController.aliasDetailLocations = jsonDataArray["carreraPorSedes"] as! NSArray
                careerAliasDetailViewController.aliasDetailId = jsonDataArray["id"] as? Int
            }
            self.navigationController?.pushViewController(careerAliasDetailViewController, animated: true)
        }
    }
}











