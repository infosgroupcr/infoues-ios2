//
//  CareersViewController.swift
//  infoUes
//
//  Created by aivatra on 7/21/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import UIKit
import CoreData
import SideMenu

struct CareerRow {
    static let headerRow = CareersHeaderTableViewCell()
    static let contentRow = CareersGenericContentTableViewCell()
}

class CareersViewController: UITableViewController {
    
    // MARK: Attributes
    
    var rowsData = [UITableViewCell]()
    var parsedEntities = [Areas]()

    // MARK: View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.estimatedRowHeight = 80
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView?.register(CareersHeaderTableViewCell.nib, forCellReuseIdentifier: CareersHeaderTableViewCell.identifier)
        tableView?.register(CareersGenericContentTableViewCell.nib, forCellReuseIdentifier: CareersGenericContentTableViewCell.identifier)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Do any additional setup after loading the view.
        let colorHelper = ColorInjectorHelper()
        let navBarColor = colorHelper.getColorFromPList(colorName: "navigationBarColorLightBlue")
        
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController!.view.backgroundColor = navBarColor
        self.navigationController?.navigationBar.backgroundColor = navBarColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        let statWindow = UIApplication.shared.value(forKey:"statusBarWindow") as! UIView
        let statusBar = statWindow.subviews[0] as UIView
        statusBar.backgroundColor = navBarColor
        self.getData()
    }
    
    // MARK: UITableViewController delegate methods
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowsData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let stringManager = StringInjectorHelper()
        var imageHeader = UIImage()
        var imageIcon = UIImage()
        var labelSkillsTextColor = UIColor()
        
        if rowsData[indexPath.row] == CareerRow.headerRow {
            // TODO: Add shortcuts collectionView
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CareersHeaderTableViewCell", for: indexPath) as! CareersHeaderTableViewCell
            cell.labelTitle.text = stringManager.getStringFromPList(sectionName: "viewCareers", stringName: "titleText")
            
            // Initialize bgcolor and image for each tile
            cell.viewButton1.backgroundColor = GenericCareerStyle.career1.careerRowColor
            cell.viewButton1.tag = 1
            cell.viewButton1.roundCorners(corners: [.topLeft], radius: 35)
            cell.viewButton1.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.scrollToCareer)))
            
            cell.viewButton2.backgroundColor = GenericCareerStyle.career2.careerRowColor
            cell.viewButton2.tag = 7
            cell.viewButton2.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.scrollToCareer)))
            
            cell.viewButton3.backgroundColor = GenericCareerStyle.career3.careerRowColor
            cell.viewButton3.tag = 6
            cell.viewButton3.roundCorners(corners: [.topRight], radius: 35)
            cell.viewButton3.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.scrollToCareer)))
            
            cell.viewButton4.backgroundColor = GenericCareerStyle.career4.careerRowColor
            cell.viewButton4.tag = 5
            cell.viewButton4.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.scrollToCareer)))
            
            cell.viewButton5.backgroundColor = GenericCareerStyle.career5.careerRowColor
            cell.viewButton5.tag = 4
            cell.viewButton5.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.scrollToCareer)))
            
            cell.viewButton6.backgroundColor = GenericCareerStyle.career6.careerRowColor
            cell.viewButton6.tag = 8
            cell.viewButton6.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.scrollToCareer)))
            
            cell.viewButton7.backgroundColor = GenericCareerStyle.career7.careerRowColor
            cell.viewButton7.tag = 3
            cell.viewButton7.roundCorners(corners: [.bottomLeft], radius: 35)
            cell.viewButton7.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.scrollToCareer)))
            
            cell.viewButton8.backgroundColor = GenericCareerStyle.career8.careerRowColor
            cell.viewButton8.tag = 9
            cell.viewButton8.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.scrollToCareer)))
            
            cell.viewButton9.backgroundColor = GenericCareerStyle.career9.careerRowColor
            cell.viewButton9.tag = 2
            cell.viewButton9.roundCorners(corners: [.bottomRight], radius: 35)
            cell.viewButton9.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.scrollToCareer)))
            
            cell.layer.backgroundColor = UIColor.clear.cgColor
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
            
        }else if rowsData[indexPath.row] == CareerRow.contentRow{
            
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CareersGenericContentTableViewCell", for: indexPath) as! CareersGenericContentTableViewCell
            
            //TODO: get images for the other 2 areas and a default
            switch parsedEntities[indexPath.row - 1].id {
                case 10.0:
                    imageHeader = GenericCareerStyle.career4.careerHeaderImage
                    imageIcon = GenericCareerStyle.career4.careerRoundedIconImage
                    labelSkillsTextColor = GenericCareerStyle.career4.careerRowColor
                case 12.0:
                    imageHeader = GenericCareerStyle.career2.careerHeaderImage
                    imageIcon = GenericCareerStyle.career2.careerRoundedIconImage
                    labelSkillsTextColor = GenericCareerStyle.career2.careerRowColor
                case 13.0:
                    imageHeader = GenericCareerStyle.career1.careerHeaderImage
                    imageIcon = GenericCareerStyle.career1.careerRoundedIconImage
                    labelSkillsTextColor = GenericCareerStyle.career1.careerRowColor
                case 14.0:
                    imageHeader = GenericCareerStyle.career8.careerHeaderImage
                    imageIcon = GenericCareerStyle.career8.careerRoundedIconImage
                    labelSkillsTextColor = GenericCareerStyle.career8.careerRowColor
                case 16.0:
                    imageHeader = GenericCareerStyle.career9.careerHeaderImage
                    imageIcon = GenericCareerStyle.career9.careerRoundedIconImage
                    labelSkillsTextColor = GenericCareerStyle.career9.careerRowColor
                case 17.0:
                    imageHeader = GenericCareerStyle.career7.careerHeaderImage
                    imageIcon = GenericCareerStyle.career7.careerRoundedIconImage
                    labelSkillsTextColor = GenericCareerStyle.career7.careerRowColor
                case 18.0:
                    imageHeader = GenericCareerStyle.career3.careerHeaderImage
                    imageIcon = GenericCareerStyle.career3.careerRoundedIconImage
                    labelSkillsTextColor = GenericCareerStyle.career3.careerRowColor
                case 20.0:
                    imageHeader = GenericCareerStyle.career5.careerHeaderImage
                    imageIcon = GenericCareerStyle.career5.careerRoundedIconImage
                    labelSkillsTextColor = GenericCareerStyle.career5.careerRowColor
                case 21.0:
                    imageHeader = GenericCareerStyle.career6.careerHeaderImage
                    imageIcon = GenericCareerStyle.career6.careerRoundedIconImage
                    labelSkillsTextColor = GenericCareerStyle.career6.careerRowColor
                default:
                    print("no cell")
            }
            
            cell.imageHeader.image = imageHeader
            cell.imageIcon.image = imageIcon
            cell.labelSkilsTitle.textColor = labelSkillsTextColor
            // The index to access the values has to be equal
            cell.rowType = parsedEntities[indexPath.row - 1].id
            // TODO: Get the value from ws
            cell.textViewSkills.text = stringManager.getStringFromPList(sectionName: "viewCareers", stringName: "textSkillsDescription")
            cell.labelSkilsTitle.text = stringManager.getStringFromPList(sectionName: "viewCareers", stringName: "titleSkills")
            cell.labelTitle.text = parsedEntities[indexPath.row - 1].nombre
            
            cell.buttonGoToCareerDetail.tag = Int(parsedEntities[indexPath.row - 1].id)
            cell.tag = indexPath.row
            cell.buttonGoToCareerDetail.addTarget(self, action: #selector(goToCareerArea(sender:)), for: .touchUpInside)
            cell.buttonUp.addTarget(self, action: #selector(goToTop), for: .touchUpInside)
            cell.layer.backgroundColor = UIColor.clear.cgColor
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            print(cell.tag)
            print(indexPath)
            
            return cell
            
        }else {
            return UITableViewCell()
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func getData() {
        let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Areas")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "nombre", ascending: true)]
        rowsData = [CareerRow.headerRow]
        do {
            let results = try context.fetch(fetchRequest)
            parsedEntities = results as! [Areas]
            for _ in results {
                rowsData += [CareerRow.contentRow]
            }
        }catch let err as NSError {
            print(err.debugDescription)
        }
    }
    
    func goToTop(){
        self.tableView.setContentOffset(CGPoint.zero, animated: true)
    }
    
    func goToCareerArea(sender: UIButton){
        let careerAreaViewController = storyboard?.instantiateViewController(withIdentifier: "CareerAreaViewController") as! CareerAreaViewController
        careerAreaViewController.careerId = Double(sender.tag)
        self.navigationController?.pushViewController(careerAreaViewController, animated: true)
    }
    
    func scrollToCareer(sender:UITapGestureRecognizer) {
        
//        if let cell = sender.view as? CareersGenericContentTableViewCell, let indexPath = self.tableView.indexPath(for: cell) {
//            print(indexPath)
//        }
        
//        // To remove background effect of cell
//        let indexPaths:Array<NSIndexPath> = self.tableView.indexPathsForSelectedRows! as Array<NSIndexPath>
//        // If you have implemented your own class which inherits from UITableViewCell, use its name while type casting and explicit typing
//        for indexPath in indexPaths {
//            let cell:UITableViewCell = self.tableView.cellForRow(at: indexPath as IndexPath) as UITableViewCell!
//            // REMOVE BACKGROUND EFFECT HERE (using cell)
//        }
        
        // This works!
//        let index = IndexPath(row: 9, section: 0)
//        self.tableView.scrollToRow(at: index, at: .top, animated: true)
        
        if let tag = sender.view?.tag {
            let indexPath = NSIndexPath(item: tag, section: 0)
            self.tableView.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
            self.tableView.reloadData()
            print(tag)
        }
    }

}

extension UIView {
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}
