//
//  CareerAliasDetailViewController.swift
//  infoUes
//
//  Created by aivatra on 8/11/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import UIKit
import CoreData

struct CareerAliasDetailRow {
    static let headerRow = CareersAliasDetailHeaderTableViewCell()
    static let contentRow = CareersAliasDetailGenericContentTableViewCell()
    static let bottomButtonsRow = CareersAliasDetailBottomButtonsTableViewCell()
}

class CareerAliasDetailViewController: UITableViewController {
    
    var aliasDetailName: String!
    var aliasDetailDescription: String!
    var aliasDetailId: Int!
    var aliasDetailLocations = NSArray()
    var rowsData = [UITableViewCell]()
    var sedeData: Sedes!
    var selectedIndexPath : IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.estimatedRowHeight = 80
        self.tableView.rowHeight = UITableViewAutomaticDimension
        tableView?.register(CareersAliasDetailHeaderTableViewCell.nib, forCellReuseIdentifier: CareersAliasDetailHeaderTableViewCell.identifier)
        tableView?.register(CareersAliasDetailGenericContentTableViewCell.nib, forCellReuseIdentifier: CareersAliasDetailGenericContentTableViewCell.identifier)
        tableView?.register(CareersAliasDetailBottomButtonsTableViewCell.nib, forCellReuseIdentifier: CareersAliasDetailBottomButtonsTableViewCell.identifier)
        self.getRows()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getRows() {
        
        rowsData = [CareerAliasDetailRow.headerRow]
        
        if let dict = aliasDetailLocations.objectEnumerator().allObjects as? [[String: Any]] {
            for _ in dict {
                rowsData += [CareerAliasDetailRow.contentRow]
            }
        }
        rowsData += [CareerAliasDetailRow.bottomButtonsRow]
    }
    
    func getData(acredId: String) -> String {
        
        let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Acreditaciones")
        let predicate = NSPredicate(format: "id == %@", String(describing: acredId))
        fetchRequest.predicate = predicate
        do {
            let results = try context.fetch(fetchRequest) as! [Acreditaciones]
            guard let image = results.first?.url else {
                return "no image found"
            }
            
            return image
            
        }catch let err as NSError {
            print(err.debugDescription)
        }
        return ""
    }
    
    func getSedeData(index: Int) -> Sedes{
        if let jsonDataArray = (aliasDetailLocations[index] as! NSDictionary) as? [String: Any]{
            
            let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Sedes")
            let predicate = NSPredicate(format: "id == %@", String((jsonDataArray["sedeId"] as? Int)!))
            fetchRequest.predicate = predicate
            
            do {
                let results = try context.fetch(fetchRequest) as! [Sedes]
                if let data = results.first {
                    sedeData = data
                    
                }
            }catch let err as NSError {
                print(err.debugDescription)
            }
            
        }
        return sedeData
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowsData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let stringManager = StringInjectorHelper()
        let colorManager = ColorInjectorHelper()
        
        if rowsData[indexPath.row] == CareerAliasDetailRow.headerRow {
            
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CareersAliasDetailHeaderTableViewCell", for: indexPath) as! CareersAliasDetailHeaderTableViewCell
            cell.labelTitle.text = aliasDetailName
            cell.labelTitle.textColor = colorManager.getColorFromPList(colorName: "textLabelLightBlue")
            cell.labelSubtitle.textColor = colorManager.getColorFromPList(colorName: "textLabelLightBlue")
            cell.textViewDescription.text = aliasDetailDescription
            cell.layer.backgroundColor = UIColor.clear.cgColor
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            return cell
            
        }else if rowsData[indexPath.row] == CareerAliasDetailRow.contentRow{
            
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CareersAliasDetailGenericContentTableViewCell", for: indexPath) as! CareersAliasDetailGenericContentTableViewCell
            
            let sede = self.getSedeData(index: indexPath.row - 1)
            cell.labelTitle.text = sede.nombre
            cell.labelSubtitle.text = stringManager.getStringFromPList(sectionName: "viewCareers", stringName: "textAcredSubtitle")
            
            if let aliasData = (aliasDetailLocations[indexPath.row - 1] as! NSDictionary) as? [String: Any]{
                if let acreditaciones = aliasData["acreditaciones"] as? String {
                    if acreditaciones.range(of: "1") != nil {
                        if let decodedData = Data(base64Encoded: getData(acredId: "1"), options: .ignoreUnknownCharacters) {
                            cell.imageAcred1.image = UIImage(data: decodedData)
                        }
                    }
                    if acreditaciones.range(of: "2") != nil {
                        if let decodedData = Data(base64Encoded: getData(acredId: "2"), options: .ignoreUnknownCharacters) {
                            cell.imageAcred2.image = UIImage(data: decodedData)
                        }
                    }
                    if acreditaciones.range(of: "3") != nil {
                        if let decodedData = Data(base64Encoded: getData(acredId: "3"), options: .ignoreUnknownCharacters) {
                            cell.imageAcred3.image = UIImage(data: decodedData)
                        }
                    }
                }
                
            }
            
            cell.imagePhone.image = #imageLiteral(resourceName: "phone")
            cell.imagePhone.isUserInteractionEnabled = true
            cell.imagePhone.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.goToContactDetail)))
            cell.imagePhone.tag = Int(sede.id)
            
            cell.imageEmail.image = #imageLiteral(resourceName: "email")
            cell.imageEmail.isUserInteractionEnabled = true
            cell.imageEmail.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.goToContactDetail)))
            cell.imageEmail.tag = Int(sede.id)
            
            cell.imageWeb.image = #imageLiteral(resourceName: "web")
            cell.imageWeb.isUserInteractionEnabled = true
            cell.imageWeb.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.goToContactDetail)))
            cell.imageWeb.tag = Int(sede.id)
            
            cell.layer.backgroundColor = UIColor.clear.cgColor
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
            
        }else
            if rowsData[indexPath.row] == CareerAliasDetailRow.bottomButtonsRow{
                if let cell = self.tableView.dequeueReusableCell(withIdentifier: "CareersAliasDetailBottomButtonsTableViewCell", for: indexPath) as? CareersAliasDetailBottomButtonsTableViewCell{
                    cell.layer.backgroundColor = UIColor.clear.cgColor
                    cell.selectionStyle = UITableViewCellSelectionStyle.none
                    return cell
                }
        }
        
        return UITableViewCell()
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if rowsData[indexPath.row] == CareerAliasDetailRow.contentRow{
            if indexPath == selectedIndexPath {
                return CareersAliasDetailGenericContentTableViewCell.expandedHeight
            }
            return CareersAliasDetailGenericContentTableViewCell.defaultHeight
        }else{
            return UITableViewAutomaticDimension
        }
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if rowsData[indexPath.row] == CareerAliasDetailRow.contentRow{
            
            let previousIndexPath = selectedIndexPath
            if indexPath == selectedIndexPath {
                selectedIndexPath = nil
            } else {
                selectedIndexPath = indexPath
            }
            
            var indexPaths : Array<IndexPath> = []
            if let previous = previousIndexPath {
                indexPaths += [previous]
            }
            if let current = selectedIndexPath {
                indexPaths += [current]
            }
            if indexPaths.count > 0 {
                tableView.reloadRows(at: indexPaths, with: UITableViewRowAnimation.automatic)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if rowsData[indexPath.row] == CareerAliasDetailRow.contentRow{
            (cell as! CareersAliasDetailGenericContentTableViewCell).watchFrameChanges()
        }
    }
    
    override func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if rowsData[indexPath.row] == CareerAliasDetailRow.contentRow{
            (cell as! CareersAliasDetailGenericContentTableViewCell).ignoreFrameChanges()
        }
    }
    
    func goToContactDetail(sender:UITapGestureRecognizer) {
        
        if let tag = sender.view?.tag {
            let storyboard = UIStoryboard(name: "Contact", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
            controller.sedeId = tag
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
}
