
//

import SideMenu
import CoreData

//enum EntityModel: String {
//    case AreasEntity: "Areas"
//    case GradosEntity: "Grados"
//    case UniversidadesEntity: "Universidades"
//    case SedesEntity: "Sedes"
//    case AcreditacionesEntity: "Acreditaciones"
//}

class MainViewController: UIViewController {
    
    @IBOutlet weak var goToGeneralInfoButton: GenericButton!
    @IBOutlet weak var generalInfoSubtitleLabel: UILabel!
    @IBOutlet weak var generalInfoSubtitleLabel2: UILabel!
    
    var labelsTextArray = [String]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController!.view.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        
//        // Test
//        
//        let yourBackImage = #imageLiteral(resourceName: "back")
//        self.navigationController?.navigationBar.backIndicatorImage = yourBackImage
//        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = yourBackImage
//        self.navigationController?.navigationBar.topItem?.title = ""
        
        let statWindow = UIApplication.shared.value(forKey:"statusBarWindow") as! UIView
        let statusBar = statWindow.subviews[0] as UIView
        statusBar.backgroundColor = UIColor.clear
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        retrieveSeccionIds()
        setupSideMenu()
        setDefaults()
        
    }
    
    // MARK: SideMenu navigation setup
    
    fileprivate func setupSideMenu() {
        // Define the menus
        SideMenuManager.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        
        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the View Controller it displays!
        SideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
//        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
    }
    
    fileprivate func setDefaults() {
        
        let stringManager = StringInjectorHelper()
        
        SideMenuManager.menuPresentMode = .menuSlideIn
        SideMenuManager.menuWidth = view.frame.width * 1.0
        SideMenuManager.menuFadeStatusBar = false
        SideMenuManager.menuBlurEffectStyle = nil
        // TODO: arreglar esto
        self.generalInfoSubtitleLabel.text = labelsTextArray[0]
        self.generalInfoSubtitleLabel2.text = labelsTextArray[1]
        self.goToGeneralInfoButton.flatButton(titleText: stringManager.getStringFromPList(sectionName: "viewLanding", stringName: "buttonLabelGeneralInfo"), titleTextColor: "buttonTextLabelWhite", buttonColor: "buttonBackgroundColorLightBlue")
    }
    
    @IBAction func goToGeneralInfoView(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "GeneralInfo", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "GeneralInfoViewController")
        self.navigationController?.pushViewController(controller, animated: true)

    }
    
    func retrieveSeccionIds() {
        
        let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Secciones")
        let predicate = NSPredicate(format: "id == %@", String(describing: 1))
        fetchRequest.predicate = predicate
        
        do {
            let results = try context.fetch(fetchRequest) as! [Secciones]
            if let data = results.first {
                for item in data.contenidos {
                    if let id = item["id"] as? Double {
                        retrieveLabelText(id: id)
                    }
                }
            }
        }catch let err as NSError {
            print(err.debugDescription)
        }
    }
    
    func retrieveLabelText(id: Double){
        let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Contenidos")
        let predicate = NSPredicate(format: "id == %@", String(describing: id))
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        fetchRequest.predicate = predicate
        
        do {
            let results = try context.fetch(fetchRequest) as! [Contenidos]
            if let data = results.first {
                if let contenido = data.contenido {
                    labelsTextArray.append(contenido)
                }
                
            }
        }catch let err as NSError {
            print(err.debugDescription)
        }

    }

}
