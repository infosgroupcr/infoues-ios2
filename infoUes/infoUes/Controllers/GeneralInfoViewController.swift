//
//  GeneralInfoViewController.swift
//  infoUes
//
//  Created by aivatra on 7/21/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import UIKit
import CoreData

struct GeneralInfoRow {
    static let headerRow = GeneralInfoHeaderTableViewCell()
    static let contentRow = GeneralInfoGenericContentTableViewCell()
    static let buttonsRow = GeneralInfoBottomButtonsTableViewCell()
}

class GeneralInfoViewController: UITableViewController {

    var rowsData = [UITableViewCell]()
    var secciones = [4, 5, 6]
    var imagesBySeccion = [#imageLiteral(resourceName: "generalInfoIcon1"), #imageLiteral(resourceName: "generalInfoIcon2"), #imageLiteral(resourceName: "generalInfoIcon3")]
    var headerTitle: String?
    var seccionData = [Secciones]()
    var selectedIndexPath : IndexPath?
    
    // MARK: View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.estimatedRowHeight = 80
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView?.register(GeneralInfoHeaderTableViewCell.nib, forCellReuseIdentifier: GeneralInfoHeaderTableViewCell.identifier)
        tableView?.register(GeneralInfoGenericContentTableViewCell.nib, forCellReuseIdentifier: GeneralInfoGenericContentTableViewCell.identifier)
        tableView?.register(GeneralInfoBottomButtonsTableViewCell.nib, forCellReuseIdentifier: GeneralInfoBottomButtonsTableViewCell.identifier)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let colorHelper = ColorInjectorHelper()
        let navBarColor = colorHelper.getColorFromPList(colorName: "navigationBarColorLightBlue")
        let bgColor = colorHelper.getColorFromPList(colorName: "viewBackgroundColorPurple")
        
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController!.view.backgroundColor = navBarColor
        self.navigationController?.navigationBar.backgroundColor = navBarColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        let statWindow = UIApplication.shared.value(forKey:"statusBarWindow") as! UIView
        let statusBar = statWindow.subviews[0] as UIView
        statusBar.backgroundColor = navBarColor
        
        tableView.backgroundColor = bgColor
        self.view.backgroundColor = bgColor
        
//        rowsData = [GeneralInfoRow.headerRow, GeneralInfoRow.contentRow, GeneralInfoRow.contentRow, GeneralInfoRow.contentRow, GeneralInfoRow.buttonsRow]
        
        self.getHeaderData()
        self.getData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UITableViewController delegate methods
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowsData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        let stringManager = StringInjectorHelper()
        
        if rowsData[indexPath.row] == GeneralInfoRow.headerRow {
            if let cell = self.tableView.dequeueReusableCell(withIdentifier: "GeneralInfoHeaderTableViewCell", for: indexPath) as? GeneralInfoHeaderTableViewCell{
//                cell.labelTitle.text = stringManager.getStringFromPList(sectionName: "viewGeneralInfo", stringName: "titleText")
                cell.labelTitle.text = headerTitle
                cell.layer.backgroundColor = UIColor.clear.cgColor
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            }
            
        }else if rowsData[indexPath.row] == GeneralInfoRow.buttonsRow {
            if let cell = self.tableView.dequeueReusableCell(withIdentifier: "GeneralInfoBottomButtonsTableViewCell", for: indexPath) as? GeneralInfoBottomButtonsTableViewCell{
                cell.layer.backgroundColor = UIColor.clear.cgColor
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            }
            
        }else if rowsData[indexPath.row] == GeneralInfoRow.contentRow{
            if let cell = self.tableView.dequeueReusableCell(withIdentifier: "GeneralInfoGenericContentTableViewCell", for: indexPath) as? GeneralInfoGenericContentTableViewCell{
                cell.labelText.text = seccionData[indexPath.row - 1].titulo
                cell.imageIcon.image = imagesBySeccion[indexPath.row - 1]
                cell.layer.backgroundColor = UIColor.clear.cgColor
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            }
        }
        
        return UITableViewCell()
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if rowsData[indexPath.row] == GeneralInfoRow.contentRow{
            if indexPath == selectedIndexPath {
                return GeneralInfoGenericContentTableViewCell.expandedHeight
            }
            return GeneralInfoGenericContentTableViewCell.defaultHeight
        }else{
            return UITableViewAutomaticDimension
        }
    }
    
    func getData() {
        
        for item in secciones {
            
            let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Secciones")
            let predicate = NSPredicate(format: "id == %@", String(item))
            fetchRequest.predicate = predicate
            
            do {
                let results = try context.fetch(fetchRequest) as! [Secciones]
                if let data = results.first {
                    seccionData.append(data)
                    rowsData += [GeneralInfoRow.contentRow]
                }
            }catch let err as NSError {
                print(err.debugDescription)
            }
            
        }
        
        rowsData += [GeneralInfoRow.buttonsRow]
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if rowsData[indexPath.row] == GeneralInfoRow.contentRow{
            
            let previousIndexPath = selectedIndexPath
            if indexPath == selectedIndexPath {
                selectedIndexPath = nil
            } else {
                selectedIndexPath = indexPath
            }
            
            var indexPaths : Array<IndexPath> = []
            if let previous = previousIndexPath {
                indexPaths += [previous]
            }
            if let current = selectedIndexPath {
                indexPaths += [current]
            }
            if indexPaths.count > 0 {
                tableView.reloadRows(at: indexPaths, with: UITableViewRowAnimation.automatic)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if rowsData[indexPath.row] == GeneralInfoRow.contentRow{
            (cell as! GeneralInfoGenericContentTableViewCell).watchFrameChanges()
        }
    }
    
    override func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if rowsData[indexPath.row] == GeneralInfoRow.contentRow{
            (cell as! GeneralInfoGenericContentTableViewCell).ignoreFrameChanges()
        }
    }
    
    func getHeaderData() {
            
        let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Secciones")
        let predicate = NSPredicate(format: "id == %@", String(3))
        fetchRequest.predicate = predicate
        
        do {
            let results = try context.fetch(fetchRequest) as! [Secciones]
            if let data = results.first {
                headerTitle = data.titulo
                rowsData += [GeneralInfoRow.headerRow]
            }
        }catch let err as NSError {
            print(err.debugDescription)
        }
    }


}
