//
//  AdmissionProcessViewController.swift
//  infoUes
//
//  Created by aivatra on 7/21/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import UIKit
import CoreData

struct AdmissionProcessRow {
    static let headerRow = AdmissionProcessHeaderTableViewCell()
    static let contentRow = AdmissionProcessTextItemTableViewCell()
    static let buttonsRow = AdmissionProcessBottomButtonsTableViewCell()
}

class AdmissionProcessViewController: UITableViewController {
    
    var rowsOrder = [UITableViewCell]()
    var labelsTextArray = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.estimatedRowHeight = 80
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView?.register(AdmissionProcessHeaderTableViewCell.nib, forCellReuseIdentifier: AdmissionProcessHeaderTableViewCell.identifier)
        tableView?.register(AdmissionProcessTextItemTableViewCell.nib, forCellReuseIdentifier: AdmissionProcessTextItemTableViewCell.identifier)
        tableView?.register(AdmissionProcessBottomButtonsTableViewCell.nib, forCellReuseIdentifier: AdmissionProcessBottomButtonsTableViewCell.identifier)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Do any additional setup after loading the view.
        let colorHelper = ColorInjectorHelper()
        let navBarColor = colorHelper.getColorFromPList(colorName: "navigationBarColorLightBlue")
        
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController!.view.backgroundColor = navBarColor
        self.navigationController?.navigationBar.backgroundColor = navBarColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        let statWindow = UIApplication.shared.value(forKey:"statusBarWindow") as! UIView
        let statusBar = statWindow.subviews[0] as UIView
        statusBar.backgroundColor = navBarColor
        
        rowsOrder = [AdmissionProcessRow.headerRow, AdmissionProcessRow.contentRow, AdmissionProcessRow.buttonsRow]
        
        self.retrieveSeccionIds()

    }
    
    // MARK: UITableViewController delegate methods
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowsOrder.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let stringManager = StringInjectorHelper()
        
        if rowsOrder[indexPath.row] == AdmissionProcessRow.headerRow {
            if let cell = self.tableView.dequeueReusableCell(withIdentifier: "AdmissionProcessHeaderTableViewCell", for: indexPath) as? AdmissionProcessHeaderTableViewCell{
                cell.labelTitle.text = stringManager.getStringFromPList(sectionName: "viewAdmissionProcess", stringName: "titleText")
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            }
            
        }else if rowsOrder[indexPath.row] == AdmissionProcessRow.buttonsRow {
            if let cell = self.tableView.dequeueReusableCell(withIdentifier: "AdmissionProcessBottomButtonsTableViewCell", for: indexPath) as? AdmissionProcessBottomButtonsTableViewCell{
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            }
            
        }else if rowsOrder[indexPath.row] == AdmissionProcessRow.contentRow{
            if let cell = self.tableView.dequeueReusableCell(withIdentifier: "AdmissionProcessTextItemTableViewCell", for: indexPath) as? AdmissionProcessTextItemTableViewCell{
//                cell.viewTextInfo.text = stringManager.getStringFromPList(sectionName: "viewAdmissionProcess", stringName: "rowDescription")
                cell.viewTextInfo.text = labelsTextArray[0]
                cell.layer.backgroundColor = UIColor.clear.cgColor
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            }
        }
        
        return UITableViewCell()
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func retrieveSeccionIds() {
        
        let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Secciones")
        let predicate = NSPredicate(format: "id == %@", String(describing: 28))
        fetchRequest.predicate = predicate
        
        do {
            let results = try context.fetch(fetchRequest) as! [Secciones]
            if let data = results.first {
                for item in data.contenidos {
                    if let id = item["id"] as? Double {
                        retrieveLabelText(id: id)
                    }
                }
            }
        }catch let err as NSError {
            print(err.debugDescription)
        }
    }
    
    func retrieveLabelText(id: Double){
        let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Contenidos")
        let predicate = NSPredicate(format: "id == %@", String(describing: id))
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        fetchRequest.predicate = predicate
        
        do {
            let results = try context.fetch(fetchRequest) as! [Contenidos]
            if let data = results.first {
                if let contenido = data.contenido {
                    labelsTextArray.append(contenido)
                }
                
            }
        }catch let err as NSError {
            print(err.debugDescription)
        }
        
    }

}
