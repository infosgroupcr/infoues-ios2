//
//  ContactViewController.swift
//  infoUes
//
//  Created by aivatra on 7/21/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreData

struct ContactRow {
    static let contentRow = ContactInfoTableViewCell()
    static let headerMapRow = MapTableViewCell()
    static let bottomButtons = ContactBottomButtonsTableViewCell()
}

class ContactViewController: UITableViewController {

//    @IBOutlet weak var googleMapsView: GMSMapView!
//    @IBOutlet weak var collectionViewTabs: UICollectionView!
//    @IBOutlet weak var tableView: UITableView!
    
    var rowsData = [UITableViewCell]()
    var sedeId: Int!
    var sedeData: Sedes!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 80
        self.tableView.isScrollEnabled = true
        self.tableView.rowHeight = UITableViewAutomaticDimension
        tableView?.register(ContactInfoTableViewCell.nib, forCellReuseIdentifier: ContactInfoTableViewCell.identifier)
        tableView?.register(ContactBottomButtonsTableViewCell.nib, forCellReuseIdentifier: ContactBottomButtonsTableViewCell.identifier)
        tableView?.register(MapTableViewCell.nib, forCellReuseIdentifier: MapTableViewCell.identifier)
        
        rowsData = [ContactRow.headerMapRow, ContactRow.contentRow, ContactRow.bottomButtons]
        
        self.getSedeData(index: sedeId)
        self.viewSetup()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Do any additional setup after loading the view.
        let colorHelper = ColorInjectorHelper()
        let navBarColor = colorHelper.getColorFromPList(colorName: "navigationBarColorLightBlue")
        
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController!.view.backgroundColor = navBarColor
        self.navigationController?.navigationBar.backgroundColor = navBarColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        let statWindow = UIApplication.shared.value(forKey:"statusBarWindow") as! UIView
        let statusBar = statWindow.subviews[0] as UIView
        statusBar.backgroundColor = navBarColor
    }
    
    func getSedeData(index: Int){
            
            let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Sedes")
            let predicate = NSPredicate(format: "id == %@", String(index))
            fetchRequest.predicate = predicate
            
            do {
                let results = try context.fetch(fetchRequest) as! [Sedes]
                if let data = results.first {
                    sedeData = data
                    
                }
            }catch let err as NSError {
                print(err.debugDescription)
            }
    }
    
    func viewSetup(){
        
//        if let latitud = sedeData.latitud, let longitud = sedeData.longitud {
//            let kCameraLatitude = Double(latitud)!
//            let kCameraLongitude = Double(longitud)!
//            let camera = GMSCameraPosition.camera(withLatitude: kCameraLatitude, longitude: kCameraLongitude, zoom: 0.1)
//            self.googleMapsView.camera = camera
//            let zoomIn = GMSCameraUpdate.zoom(to: 15)
//            self.googleMapsView.animate(with: zoomIn)
//            let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: kCameraLatitude, longitude: kCameraLongitude))
//            marker.map = self.googleMapsView
//        }

    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowsData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if rowsData[indexPath.row] == ContactRow.headerMapRow {
            
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "MapTableViewCell", for: indexPath) as! MapTableViewCell
            
            if let latitud = sedeData.latitud, let longitud = sedeData.longitud {
                let kCameraLatitude = Double(latitud)!
                let kCameraLongitude = Double(longitud)!
                let camera = GMSCameraPosition.camera(withLatitude: kCameraLatitude, longitude: kCameraLongitude, zoom: 0.1)
                cell.mapView.camera = camera
                let zoomIn = GMSCameraUpdate.zoom(to: 15)
                cell.mapView.animate(with: zoomIn)
                let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: kCameraLatitude, longitude: kCameraLongitude))
                marker.map = cell.mapView
            }
            
            return cell
            
        }else if rowsData[indexPath.row] == ContactRow.contentRow {
            
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "ContactInfoTableViewCell", for: indexPath) as! ContactInfoTableViewCell
            
            if let name = sedeData.nombre, let tel = sedeData.tel, let email = sedeData.email, let url = sedeData.url {
                let colorManager = ColorInjectorHelper()
                cell.labelTitle.text = name
                cell.labelTitle.textColor = colorManager.getColorFromPList(colorName: "textLabelLightBlue")
                cell.labelPhone.text = "Telefono: \(String(describing: tel))"
                cell.labelEmail.text = "Correo electrónico: \(String(describing: email))"
                cell.labelWeb.text = "Sitio web: \(String(describing: url))"
                
            }
            cell.layer.backgroundColor = UIColor.clear.cgColor
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
            
        }else if rowsData[indexPath.row] == ContactRow.bottomButtons {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "ContactBottomButtonsTableViewCell", for: indexPath) as! ContactBottomButtonsTableViewCell
            cell.layer.backgroundColor = UIColor.clear.cgColor
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
        return UITableViewCell()
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }

}
