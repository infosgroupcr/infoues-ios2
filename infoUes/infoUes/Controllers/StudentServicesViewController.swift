//
//  StudentServicesViewController.swift
//  infoUes
//
//  Created by aivatra on 7/21/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import UIKit

struct StudentServicesRow {
    static let headerRow = StudentServicesHeaderTableViewCell()
    static let contentRow = StudentServicesGenericContentTableViewCell()
    static let buttonsRow = StudentServicesBottomButtonsTableViewCell()
}

class StudentServicesViewController: UITableViewController {
    
    var rowsData = [UITableViewCell]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.tableView.estimatedRowHeight = 80
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView?.register(StudentServicesHeaderTableViewCell.nib, forCellReuseIdentifier: StudentServicesHeaderTableViewCell.identifier)
        tableView?.register(StudentServicesGenericContentTableViewCell.nib, forCellReuseIdentifier: StudentServicesGenericContentTableViewCell.identifier)
        tableView?.register(StudentServicesBottomButtonsTableViewCell.nib, forCellReuseIdentifier: StudentServicesBottomButtonsTableViewCell.identifier)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Do any additional setup after loading the view.
        let colorHelper = ColorInjectorHelper()
        let navBarColor = colorHelper.getColorFromPList(colorName: "navigationBarColorLightBlue")
        
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController!.view.backgroundColor = navBarColor
        self.navigationController?.navigationBar.backgroundColor = navBarColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        let statWindow = UIApplication.shared.value(forKey:"statusBarWindow") as! UIView
        let statusBar = statWindow.subviews[0] as UIView
        statusBar.backgroundColor = navBarColor
        
        rowsData = [StudentServicesRow.headerRow, StudentServicesRow.contentRow, StudentServicesRow.contentRow, StudentServicesRow.contentRow, StudentServicesRow.contentRow, StudentServicesRow.contentRow, StudentServicesRow.contentRow, StudentServicesRow.buttonsRow]
    }
    
    // MARK: UITableViewController delegate methods
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowsData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let stringManager = StringInjectorHelper()
        
        if rowsData[indexPath.row] == StudentServicesRow.headerRow {
            if let cell = self.tableView.dequeueReusableCell(withIdentifier: "StudentServicesHeaderTableViewCell", for: indexPath) as? StudentServicesHeaderTableViewCell{
                cell.labelTitle.text = stringManager.getStringFromPList(sectionName: "viewStudentServices", stringName: "titleText")
                cell.layer.backgroundColor = UIColor.clear.cgColor
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            }
            
        }else if rowsData[indexPath.row] == StudentServicesRow.buttonsRow {
            if let cell = self.tableView.dequeueReusableCell(withIdentifier: "StudentServicesBottomButtonsTableViewCell", for: indexPath) as? StudentServicesBottomButtonsTableViewCell{
                cell.layer.backgroundColor = UIColor.clear.cgColor
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            }
            
        }else if rowsData[indexPath.row] == StudentServicesRow.contentRow{
            if let cell = self.tableView.dequeueReusableCell(withIdentifier: "StudentServicesGenericContentTableViewCell", for: indexPath) as? StudentServicesGenericContentTableViewCell{
                //                cell.viewTextInfo.text = stringManager.getStringFromPList(sectionName: "viewAdmissionProcess", stringName: "rowDescription")
                cell.layer.backgroundColor = UIColor.clear.cgColor
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            }
        }
        
        return UITableViewCell()
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
