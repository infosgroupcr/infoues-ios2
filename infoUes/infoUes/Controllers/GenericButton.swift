//
//  GenericButton.swift
//  infoUes
//
//  Created by aivatra on 8/6/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import UIKit

class GenericButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    private var titleText: String!
    private var titleTextColor: String!
    private var buttonColor: String!

    convenience init(titleText: String, titleTextColor: String, buttonColor: String){
        self.init()
        self.titleText = titleText
        self.titleTextColor = titleTextColor
        self.buttonColor = buttonColor
        
        flatButton(titleText: self.titleText, titleTextColor: self.titleTextColor, buttonColor: self.buttonColor)
        
    }
    
    func flatButton(titleText: String, titleTextColor: String, buttonColor: String) {
        
        let colorManager = ColorInjectorHelper()
        
        self.setTitle(titleText, for: .normal)
        self.backgroundColor = colorManager.getColorFromPList(colorName: buttonColor)
        self.layer.cornerRadius = 25
        self.clipsToBounds = true
        self.titleLabel?.font = UIFont.systemFont(ofSize: 22)
        self.titleLabel?.textColor = colorManager.getColorFromPList(colorName: titleTextColor)
        self.tintColor = colorManager.getColorFromPList(colorName: titleTextColor)
//        self.frame = CGRect(x: 100, y: 100, width: 255, height: 53)
        
        // all constaints
        let widthContraints =  NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 255)
        let heightContraints = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 53)
//        let xContraints = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: self.superview, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0)
//        let yContraints = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: self.superview, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0)
        NSLayoutConstraint.activate([heightContraints,widthContraints])
        
    }
    
    func attributedButton(titleText: String, titleTextColor: String, buttonColor: String, borderColor: String, image: UIImage) {
        
        let colorManager = ColorInjectorHelper()
        self.setTitle(titleText, for: .normal)
        self.backgroundColor = colorManager.getColorFromPList(colorName: buttonColor)
        self.layer.cornerRadius = 25
        self.clipsToBounds = true
        self.titleLabel?.font = UIFont.systemFont(ofSize: 22)
        self.titleLabel?.textColor = colorManager.getColorFromPList(colorName: titleTextColor)
        self.tintColor = colorManager.getColorFromPList(colorName: titleTextColor)
        self.layer.borderWidth = 2.0
        self.layer.borderColor = colorManager.getColorFromPList(colorName: borderColor).cgColor
        self.setImage(image, for: .normal)
        self.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -275)
        
        // all constaints
        let widthContraints =  NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 255)
        let heightContraints = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 53)
        //        let xContraints = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: self.superview, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0)
        //        let yContraints = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: self.superview, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0)
        NSLayoutConstraint.activate([heightContraints,widthContraints])
        
    }
    
    func goToTopButton(titleText: String, titleTextColor: String, buttonColor: String, image: UIImage){
        let colorManager = ColorInjectorHelper()
        self.setTitle(titleText, for: .normal)
        self.backgroundColor = colorManager.getColorFromPList(colorName: buttonColor)
        self.layer.cornerRadius = 25
        self.clipsToBounds = true
        self.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        self.titleLabel?.textColor = colorManager.getColorFromPList(colorName: titleTextColor)
        self.tintColor = colorManager.getColorFromPList(colorName: titleTextColor)
        self.setImage(image, for: .normal)
        self.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 15)
        
        // all constaints
        let widthContraints =  NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 100)
        let heightContraints = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 30)
        //        let xContraints = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: self.superview, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0)
        //        let yContraints = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: self.superview, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0)
        NSLayoutConstraint.activate([heightContraints,widthContraints])
    }
    
}
