//
//  CareerAreaViewController.swift
//  infoUes
//
//  Created by aivatra on 8/7/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import UIKit
import CoreData

struct CareerAreaRow {
    static let headerRow = CareersAreaHeaderTableViewCell()
    static let contentRow = CareersAreaGenericContentTableViewCell()
    static let bottomButtonsRow = CareersAreaBottomButtonsTableViewCell()
}

class CareerAreaViewController: UITableViewController {

    var careerId: Double?
    var areaData: Areas!
    var rowsData = [UITableViewCell]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.estimatedRowHeight = 80
        self.tableView.rowHeight = UITableViewAutomaticDimension
        tableView?.register(CareersAreaHeaderTableViewCell.nib, forCellReuseIdentifier: CareersAreaHeaderTableViewCell.identifier)
        tableView?.register(CareersAreaGenericContentTableViewCell.nib, forCellReuseIdentifier: CareersAreaGenericContentTableViewCell.identifier)
        tableView?.register(CareersAreaBottomButtonsTableViewCell.nib, forCellReuseIdentifier: CareersAreaBottomButtonsTableViewCell.identifier)
        print("Career Id passed is: \(String(describing: self.careerId))")
        self.getData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getData() {
        
        rowsData = [CareerAreaRow.headerRow]
        
        let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Areas")
        let predicate = NSPredicate(format: "id == %@", String(describing: self.careerId!))
        fetchRequest.predicate = predicate
        do {
            let results = try context.fetch(fetchRequest) as! [Areas]
//            print(results.first?.aliases as! [[String: Any]])
            if let data = results.first {
                areaData = data
                
                for _ in areaData.aliases{
                    rowsData += [CareerAreaRow.contentRow]
//                    if let aliasName = item["nombre"], let aliasId = item["id"], let aliasUniByCareer = item["carreraUniversidads"] {
////                        print(aliasName) // Returns a dictionary by key/value
//                        print(aliasId)
//                        print(aliasUniByCareer)

//                    }
                }
                rowsData += [CareerAreaRow.bottomButtonsRow]
                
            }
        }catch let err as NSError {
            print(err.debugDescription)
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowsData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let stringManager = StringInjectorHelper()
        let colorManager = ColorInjectorHelper()
        var imageHeader = UIImage()
        var generalColor = UIColor()
        
        //TODO: get images for the other 2 areas and a default
        switch areaData.id {
        case 10.0:
            imageHeader = GenericCareerStyle.career4.careerHeaderImage
            generalColor = GenericCareerStyle.career4.careerRowColor
        case 12.0:
            imageHeader = GenericCareerStyle.career2.careerHeaderImage
            generalColor = GenericCareerStyle.career2.careerRowColor
        case 13.0:
            imageHeader = GenericCareerStyle.career1.careerHeaderImage
            generalColor = GenericCareerStyle.career1.careerRowColor
        case 14.0:
            imageHeader = GenericCareerStyle.career8.careerHeaderImage
            generalColor = GenericCareerStyle.career8.careerRowColor
        case 16.0:
            imageHeader = GenericCareerStyle.career9.careerHeaderImage
            generalColor = GenericCareerStyle.career9.careerRowColor
        case 17.0:
            imageHeader = GenericCareerStyle.career7.careerHeaderImage
            generalColor = GenericCareerStyle.career7.careerRowColor
        case 18.0:
            imageHeader = GenericCareerStyle.career3.careerHeaderImage
            generalColor = GenericCareerStyle.career3.careerRowColor
        case 20.0:
            imageHeader = GenericCareerStyle.career5.careerHeaderImage
            generalColor = GenericCareerStyle.career5.careerRowColor
        case 21.0:
            imageHeader = GenericCareerStyle.career6.careerHeaderImage
            generalColor = GenericCareerStyle.career6.careerRowColor
        default:
            print("no cell")
        }
        
        if rowsData[indexPath.row] == CareerAreaRow.headerRow {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CareersAreaHeaderTableViewCell", for: indexPath) as! CareersAreaHeaderTableViewCell
            cell.labelTitleText.text = areaData.nombre
            cell.labelTitleText.textColor = colorManager.getColorFromPList(colorName: "textLabelWhite")
            cell.imageHeader.image = imageHeader
            cell.labelSubtitleText.text = stringManager.getStringFromPList(sectionName: "viewCareers", stringName: "textSubtitleAliases")
            cell.labelSubtitleText.textColor = generalColor
            cell.layer.backgroundColor = UIColor.clear.cgColor
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            return cell
            
        }else if rowsData[indexPath.row] == CareerAreaRow.contentRow{
            
            // TODO: Fix row style and figure out a way to display the universities images this career belongs to
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CareersAreaGenericContentTableViewCell", for: indexPath) as! CareersAreaGenericContentTableViewCell
            
            if let aliasName = areaData.aliases[indexPath.row - 1]["nombre"]{
                cell.labelTitleText.text = aliasName as? String
            }
            
            cell.layer.backgroundColor = UIColor.clear.cgColor
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
            
        }else if rowsData[indexPath.row] == CareerAreaRow.bottomButtonsRow{
            if let cell = self.tableView.dequeueReusableCell(withIdentifier: "CareersAreaBottomButtonsTableViewCell", for: indexPath) as? CareersAreaBottomButtonsTableViewCell{
                //                cell.viewTextInfo.text = stringManager.getStringFromPList(sectionName: "viewAdmissionProcess", stringName: "rowDescription")
                cell.layer.backgroundColor = UIColor.clear.cgColor
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            }
        }
        
        return UITableViewCell()

    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if rowsData[indexPath.row] == CareerAreaRow.contentRow{
            //        NSLog("You selected cell number: \(String(describing: areaData.aliases[indexPath.row - 1]["id"]))!")
            let careerAliasViewController = storyboard?.instantiateViewController(withIdentifier: "CareerAliasViewController") as! CareerAliasViewController
            careerAliasViewController.careeAliasId = areaData.aliases[indexPath.row - 1]["id"] as? Double
            careerAliasViewController.careerAliasName = areaData.aliases[indexPath.row - 1]["nombre"] as! String
            careerAliasViewController.universitiesByCareer = areaData.aliases[indexPath.row - 1]["carreraUniversidads"] as! NSArray
            self.navigationController?.pushViewController(careerAliasViewController, animated: true)
        }
    }
}





























