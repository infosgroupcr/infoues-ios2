//
//  ImageConverterHelper.swift
//  infoUes
//
//  Created by aivatra on 8/12/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import Foundation

import Foundation
import UIKit

class ImageConverterHelper {
    
    static let sharedInstance = ImageConverterHelper()
    
    init() {}
    
    func convertImage(imageString: String) -> UIImage {
        if let decodedData = Data(base64Encoded: imageString, options: .ignoreUnknownCharacters) {
            let image = UIImage(data: decodedData)
            return image!
        }
        return UIImage()
    }
}
