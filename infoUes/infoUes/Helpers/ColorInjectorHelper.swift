//
//  ColorInjectorHelper.swift
//  infoUes
//
//  Created by aivatra on 8/2/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import Foundation
import UIKit

class ColorInjectorHelper {
    
    static let sharedInstance = ColorInjectorHelper()
    
    init() {
        //
    }
    
    func getColorFromPList(colorName: String) -> UIColor {
        if let path = Bundle.main.path(forResource: "Colors", ofType: "plist"), let dict = NSDictionary(contentsOfFile: path) as? [String: AnyObject] {
            // use swift dictionary as normal
            for (key, value) in dict {
                if key == colorName {
                    return UIColor(hex: value as! String)
                }
            }
            
        }
        return UIColor.clear
    }
    
}

extension UIColor {
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
}
