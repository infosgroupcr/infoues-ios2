//
//  API.swift
//  infoUes
//
//  Created by aivatra on 7/19/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import Foundation

enum Result <T>{
    case Success(T)
    case Error(String)
}

class APIService: NSObject {
    
    // MARK: Constants
    
    fileprivate let apiUrl = Bundle.main.infoDictionary!["API_ENDPOINT"] as! String
    
    func getData(completion: @escaping (Result<[String: AnyObject]>) -> Void) {
        guard let url = URL(string:apiUrl + "infoues" ) else { return completion(.Error("Invalid URL, we can't update your feed")) }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard error == nil, let data = data else { return completion(.Error(error!.localizedDescription)) }
            
                do {
                    guard let json = try JSONSerialization.jsonObject(with: data, options: [.mutableContainers]) as? [String: AnyObject] else {
                        return completion(.Error(error?.localizedDescription ?? "There are no new Items to show"))
                    }
                    DispatchQueue.main.async {
                        completion(.Success(json))
                        UserDefaults.standard.set(Date(), forKey:"lastTimeDataSaved")
                    }
                } catch let error {
                    return completion(.Error(error.localizedDescription))
                }
            
            }.resume()
    }
    
    func getChecksum(completion: @escaping (Result<[String: AnyObject]>) -> Void) {
        guard let url = URL(string:apiUrl + "checksum" ) else { return completion(.Error("Invalid URL, we can't update your feed")) }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard error == nil, let data = data else { return completion(.Error(error!.localizedDescription)) }
            
            do {
                guard let json = try JSONSerialization.jsonObject(with: data, options: [.mutableContainers]) as? [String: AnyObject] else {
                    return completion(.Error(error?.localizedDescription ?? "There are no new Items to show"))
                }
                DispatchQueue.main.async {
                    completion(.Success(json))
                }
            } catch let error {
                return completion(.Error(error.localizedDescription))
            }
            
            }.resume()
    }
    
}
