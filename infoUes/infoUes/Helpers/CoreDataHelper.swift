//
//  CoreDataHelper.swift
//  infoUes
//
//  Created by aivatra on 7/26/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import Foundation
import CoreData

class CoreDataHelper: NSObject {

    // MARK: Core Data operations
    
    override init(){}
    
    private func clearData(itemType: String) {
        do {
            let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: itemType)
            do {
                let objects  = try context.fetch(fetchRequest) as? [NSManagedObject]
                _ = objects.map{$0.map{context.delete($0)}}
                CoreDataStack.sharedInstance.saveContext()
            } catch let error {
                print("Error deleting: \(error)")
            }
        }
    }
    
    public func saveInCoreDataWith(array: [String: Any]) {
        
        let arrayKeys = array.keys
        
        self.clearData(itemType: "Acreditaciones")
        self.clearData(itemType: "Sedes")
        self.clearData(itemType: "Grados")
        self.clearData(itemType: "Universidades")
        self.clearData(itemType: "Areas")
        self.clearData(itemType: "Secciones")
        self.clearData(itemType: "Contenidos")
        
        for key in arrayKeys {
//            print("parent node: \(key)")
            guard let dataFromArray = array[key] as? [[String: AnyObject]] else {
                return print("no data for node: \(key)")
            }
            for dict in dataFromArray {
                _ = self.createEntitiesFromDictionary(dictionary: dict, itemType: key)
            }
            
            do {
                try CoreDataStack.sharedInstance.persistentContainer.viewContext.save()
            } catch let error {
                print(error)
            }
        }
    }
    
    // MARK: Data parsing
    
    private func createEntitiesFromDictionary(dictionary: [String: Any], itemType: String) -> NSManagedObject? {
        
        let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
        print("\(itemType) data: \(dictionary)")
        
        switch itemType {
        case "acreditaciones":
            if let acreditacionesEntity = NSEntityDescription.insertNewObject(forEntityName: "Acreditaciones", into: context) as? Acreditaciones {
                acreditacionesEntity.id = (dictionary["id"] as? Double)!
                acreditacionesEntity.nombre = dictionary["nombre"] as? String
                acreditacionesEntity.url = dictionary["url"] as? String
                return acreditacionesEntity
            }
        case "sedes":
            if let sedesEntity = NSEntityDescription.insertNewObject(forEntityName: "Sedes", into: context) as? Sedes {
                sedesEntity.id = (dictionary["id"] as? Double)!
                sedesEntity.email = dictionary["email"] as? String
                sedesEntity.nombre = dictionary["nombre"] as? String
                sedesEntity.tel = dictionary["tel"] as? String
                sedesEntity.ubicacion = dictionary["ubicacion"] as? String
                sedesEntity.url = dictionary["url"] as? String
                sedesEntity.latitud = dictionary["latitud"] as? String
                sedesEntity.longitud = dictionary["longitud"] as? String
                return sedesEntity
            }
        case "grados":
            if let gradosEntity = NSEntityDescription.insertNewObject(forEntityName: "Grados", into: context) as? Grados {
                gradosEntity.id = (dictionary["id"] as? Double)!
                gradosEntity.categoria = dictionary["categoria"] as? String
                gradosEntity.nombre = dictionary["nombre"] as? String
                return gradosEntity
            }
        case "universidades":
            if let universidadesEntity = NSEntityDescription.insertNewObject(forEntityName: "Universidades", into: context) as? Universidades {
                universidadesEntity.id = (dictionary["id"] as? Double)!
                universidadesEntity.sedes = dictionary["sedes"] as! [[String : Any]]
                universidadesEntity.descripcion = dictionary["descripcion"] as? String
                universidadesEntity.foto = dictionary["foto"] as? String
                universidadesEntity.nombre = dictionary["nombre"] as? String
                universidadesEntity.url = dictionary["url"] as? String
                return universidadesEntity
            }
        case "areas":
            if let areasEntity = NSEntityDescription.insertNewObject(forEntityName: "Areas", into: context) as? Areas {
                areasEntity.id = (dictionary["id"] as? Double)!
                areasEntity.aliases = dictionary["aliases"] as! [[String : Any]]
                areasEntity.nombre = dictionary["nombre"] as? String
                return areasEntity
            }
        case "secciones":
            if let seccionesEntity = NSEntityDescription.insertNewObject(forEntityName: "Secciones", into: context) as? Secciones {
                seccionesEntity.id = (dictionary["id"] as? Double)!
                seccionesEntity.contenidos = dictionary["contenidos"] as! [[String : Any]]
                seccionesEntity.nombre = dictionary["nombre"] as? String
                seccionesEntity.titulo = dictionary["titulo"] as? String
                return seccionesEntity
            }
        case "contenidos":
            if let contenidosEntity = NSEntityDescription.insertNewObject(forEntityName: "Contenidos", into: context) as? Contenidos {
                contenidosEntity.id = (dictionary["id"] as? Double)!
                contenidosEntity.contenido = dictionary["contenido"] as? String
                contenidosEntity.orden = (dictionary["orden"] as? Double)!
                return contenidosEntity
            }
        default:
            return nil
        }
        
        return nil
    }
    
}
