//
//  StringInjectorHelper.swift
//  infoUes
//
//  Created by aivatra on 8/6/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import Foundation
import UIKit

class StringInjectorHelper {
    
    static let sharedInstance = StringInjectorHelper()
    
    init() {
        //
    }
    
    func getStringFromPList(sectionName: String, stringName: String) -> String {
        if let path = Bundle.main.path(forResource: "Strings", ofType: "plist"), let dict = NSDictionary(contentsOfFile: path) as? [String: AnyObject] {
            // use swift dictionary as normal
            for (key, value) in dict {

                if key == sectionName {
//                    print(value[stringName])
                    return value[stringName] as! String!
                }
            }
            
        }
        return "String not found"
    }
    
    func getArrayFromPList(sectionName: String, stringName: String) -> [String] {
        if let path = Bundle.main.path(forResource: "Strings", ofType: "plist"), let dict = NSDictionary(contentsOfFile: path) as? [String: AnyObject] {
            // use swift dictionary as normal
            for (key, value) in dict {
                
                if key == sectionName {
                    return value[stringName] as! [String]!
                }
            }
            
        }
        return []
    }
    
}
