//
//  ImageMapper.swift
//  infoUes
//
//  Created by aivatra on 8/10/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import Foundation
import UIKit

enum GenericCareerStyle {
    case career1
    case career2
    case career3
    case career4
    case career5
    case career6
    case career7
    case career8
    case career9
    
    var careerChevronImageRight: UIImage {
        switch self {
        case .career1:
            return UIImage(named: "careerChevronRight1")!
        case .career2:
            return UIImage(named: "careerChevronRight2")!
        case .career3:
            return UIImage(named: "careerChevronRight3")!
        case .career4:
            return UIImage(named: "careerChevronRight4")!
        case .career5:
            return UIImage(named: "careerChevronRight5")!
        case .career6:
            return UIImage(named: "careerChevronRight6")!
        case .career7:
            return UIImage(named: "careerChevronRight7")!
        case .career8:
            return UIImage(named: "careerChevronRight8")!
        case .career9:
            return UIImage(named: "careerChevronRight9")!
        }
    }
    
    var careerChevronImageUp: UIImage {
        switch self {
        case .career1:
            return UIImage(named: "careerChevron1")!
        case .career2:
            return UIImage(named: "careerChevron2")!
        case .career3:
            return UIImage(named: "careerChevron3")!
        case .career4:
            return UIImage(named: "careerChevron4")!
        case .career5:
            return UIImage(named: "careerChevron5")!
        case .career6:
            return UIImage(named: "careerChevron6")!
        case .career7:
            return UIImage(named: "careerChevron7")!
        case .career8:
            return UIImage(named: "careerChevron8")!
        case .career9:
            return UIImage(named: "careerChevron9")!
        }
    }
    
    var careerHeaderImage: UIImage {
        switch self {
        case .career1:
            return UIImage(named: "careerHeader1")!
        case .career2:
            return UIImage(named: "careerHeader2")!
        case .career3:
            return UIImage(named: "careerHeader3")!
        case .career4:
            return UIImage(named: "careerHeader4")!
        case .career5:
            return UIImage(named: "careerHeader5")!
        case .career6:
            return UIImage(named: "careerHeader6")!
        case .career7:
            return UIImage(named: "careerHeader7")!
        case .career8:
            return UIImage(named: "careerHeader8")!
        case .career9:
            return UIImage(named: "careerHeader9")!
        }
    }
    
    var careerRoundedIconImage: UIImage {
        switch self {
        case .career1:
            return UIImage(named: "careersIcon1Large")!
        case .career2:
            return UIImage(named: "careersIcon2Large")!
        case .career3:
            return UIImage(named: "careersIcon3Large")!
        case .career4:
            return UIImage(named: "careersIcon4Large")!
        case .career5:
            return UIImage(named: "careersIcon5Large")!
        case .career6:
            return UIImage(named: "careersIcon6Large")!
        case .career7:
            return UIImage(named: "careersIcon7Large")!
        case .career8:
            return UIImage(named: "careersIcon8Large")!
        case .career9:
            return UIImage(named: "careersIcon9Large")!
        }
    }
    
    var careerRowColor: UIColor {
        let colorInjector = ColorInjectorHelper()
        switch self {
        case .career1:
            return colorInjector.getColorFromPList(colorName: "careerGeneralColor1")
        case .career2:
            return colorInjector.getColorFromPList(colorName: "careerGeneralColor2")
        case .career3:
            return colorInjector.getColorFromPList(colorName: "careerGeneralColor3")
        case .career4:
            return colorInjector.getColorFromPList(colorName: "careerGeneralColor4")
        case .career5:
            return colorInjector.getColorFromPList(colorName: "careerGeneralColor5")
        case .career6:
            return colorInjector.getColorFromPList(colorName: "careerGeneralColor6")
        case .career7:
            return colorInjector.getColorFromPList(colorName: "careerGeneralColor7")
        case .career8:
            return colorInjector.getColorFromPList(colorName: "careerGeneralColor8")
        case .career9:
            return colorInjector.getColorFromPList(colorName: "careerGeneralColor9")
        }
    }
}
