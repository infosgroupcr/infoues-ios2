//
//  Acreditaciones+CoreDataProperties.swift
//  
//
//  Created by aivatra on 7/19/17.
//
//

import Foundation
import CoreData


extension Acreditaciones {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Acreditaciones> {
        return NSFetchRequest<Acreditaciones>(entityName: "Acreditaciones")
    }

    @NSManaged public var id: Double
    @NSManaged public var nombre: String?
    @NSManaged public var url: String?

}
