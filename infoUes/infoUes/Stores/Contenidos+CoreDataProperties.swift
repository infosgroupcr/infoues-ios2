//
//  Contenidos+CoreDataProperties.swift
//  infoUes
//
//  Created by aivatra on 8/20/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import Foundation
import CoreData


extension Contenidos {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Contenidos> {
        return NSFetchRequest<Contenidos>(entityName: "Contenidos")
    }

    @NSManaged public var id: Double
    @NSManaged public var contenido: String?
    @NSManaged public var orden: Double

}
