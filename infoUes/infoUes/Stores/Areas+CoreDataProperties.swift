//
//  Areas+CoreDataProperties.swift
//  
//
//  Created by aivatra on 7/19/17.
//
//

import Foundation
import CoreData


extension Areas {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Areas> {
        return NSFetchRequest<Areas>(entityName: "Areas")
    }

    @NSManaged public var id: Double
    @NSManaged public var nombre: String?
    @NSManaged public var aliases: [[String: Any]]

}
