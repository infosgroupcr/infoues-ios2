//
//  Secciones+CoreDataProperties.swift
//  infoUes
//
//  Created by aivatra on 8/20/17.
//  Copyright © 2017 aivatradev. All rights reserved.
//

import Foundation
import CoreData


extension Secciones {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Secciones> {
        return NSFetchRequest<Secciones>(entityName: "Secciones")
    }

    @NSManaged public var id: Double
    @NSManaged public var nombre: String?
    @NSManaged public var titulo: String?
    @NSManaged public var contenidos: [[String: Any]]

}
