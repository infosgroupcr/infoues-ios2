//
//  Grados+CoreDataProperties.swift
//  
//
//  Created by aivatra on 7/19/17.
//
//

import Foundation
import CoreData


extension Grados {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Grados> {
        return NSFetchRequest<Grados>(entityName: "Grados")
    }

    @NSManaged public var id: Double
    @NSManaged public var nombre: String?
    @NSManaged public var categoria: String?

}
