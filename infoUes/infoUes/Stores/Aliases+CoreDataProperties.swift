//
//  Aliases+CoreDataProperties.swift
//  
//
//  Created by aivatra on 7/19/17.
//
//

import Foundation
import CoreData


extension Aliases {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Aliases> {
        return NSFetchRequest<Aliases>(entityName: "Aliases")
    }

    @NSManaged public var id: Double
    @NSManaged public var nombre: String?
    @NSManaged public var carrerasUniversidads: NSObject?
    @NSManaged public var carrerasUniversidadsPorAlias: NSSet?

}

// MARK: Generated accessors for carrerasUniversidadsPorAlias
extension Aliases {

    @objc(addCarrerasUniversidadsPorAliasObject:)
    @NSManaged public func addToCarrerasUniversidadsPorAlias(_ value: CarrerasUniversidads)

    @objc(removeCarrerasUniversidadsPorAliasObject:)
    @NSManaged public func removeFromCarrerasUniversidadsPorAlias(_ value: CarrerasUniversidads)

    @objc(addCarrerasUniversidadsPorAlias:)
    @NSManaged public func addToCarrerasUniversidadsPorAlias(_ values: NSSet)

    @objc(removeCarrerasUniversidadsPorAlias:)
    @NSManaged public func removeFromCarrerasUniversidadsPorAlias(_ values: NSSet)

}
