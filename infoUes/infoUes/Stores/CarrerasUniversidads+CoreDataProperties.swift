//
//  CarrerasUniversidads+CoreDataProperties.swift
//  
//
//  Created by aivatra on 7/19/17.
//
//

import Foundation
import CoreData


extension CarrerasUniversidads {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CarrerasUniversidads> {
        return NSFetchRequest<CarrerasUniversidads>(entityName: "CarrerasUniversidads")
    }

    @NSManaged public var id: Double
    @NSManaged public var nombre: String?
    @NSManaged public var descripcion: String?
    @NSManaged public var tel: String?
    @NSManaged public var email: String?
    @NSManaged public var url: String?
    @NSManaged public var idUniversidad: Double
    @NSManaged public var grados: NSObject?
    @NSManaged public var carreraPorSedes: NSObject?

}
