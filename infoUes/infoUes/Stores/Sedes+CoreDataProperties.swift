//
//  Sedes+CoreDataProperties.swift
//  
//
//  Created by aivatra on 7/19/17.
//
//

import Foundation
import CoreData


extension Sedes {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Sedes> {
        return NSFetchRequest<Sedes>(entityName: "Sedes")
    }

    @NSManaged public var id: Double
    @NSManaged public var nombre: String?
    @NSManaged public var tel: String?
    @NSManaged public var url: String?
    @NSManaged public var email: String?
    @NSManaged public var ubicacion: String?
    @NSManaged public var latitud: String?
    @NSManaged public var longitud: String?

}
