//
//  Universidades+CoreDataProperties.swift
//  
//
//  Created by aivatra on 7/19/17.
//
//

import Foundation
import CoreData


extension Universidades {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Universidades> {
        return NSFetchRequest<Universidades>(entityName: "Universidades")
    }

    @NSManaged public var id: Double
    @NSManaged public var nombre: String?
    @NSManaged public var descripcion: String?
    @NSManaged public var foto: String?
    @NSManaged public var url: String?
    @NSManaged public var sedes: [[String: Any]]

}
